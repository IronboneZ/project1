using System;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SpawnAtGame2 : MonoBehaviour
{
    [SerializeField] private Button startButton;
    [SerializeField] private RectTransform dialog;
    [SerializeField] private PlayerSpaceship playerSpaceship;
    [SerializeField] private EnemySpaceship enemySpaceship;
    [SerializeField] private int playerSpaceshipHp;
    [SerializeField] private int playerSpaceshipMoveSpeed;
    [SerializeField] private int enemySpaceshipHp;
    [SerializeField] private int enemySpaceshipMoveSpeed;
    
    
    public PlayerSpaceship spawnedPlayerShip;
    
    private void Awake()
    {
        startButton.onClick.AddListener(OnStartButtonClicked);
    }
    
    private void OnStartButtonClicked()
    {
        dialog.gameObject.SetActive(false);
        StartGame();
    }
    
    private void StartGame()
    {
        
        SpawnPlayerSpaceship();
        SpawnEnemySpaceship();
    }
    
    

    private void OnPlayerSpaceshipExploded()
    {
        SceneManager.LoadScene(3);
    }

    
    private void SpawnEnemySpaceship()
    {
        var spawnedEnemyShip = Instantiate(enemySpaceship);
        spawnedEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
        spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
    }

    private void OnEnemySpaceshipExploded()
    {
        SceneManager.LoadScene(2);
    }
    
    private void SpawnPlayerSpaceship()
    {
        spawnedPlayerShip = Instantiate(playerSpaceship);
        spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
        spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
    }
    
    private void Restart()
    {
        
        
    }
}
