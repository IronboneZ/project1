using System;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private AudioClip playerExplodeSound;
        [SerializeField] private float playerFireSoundVolume = 0.2f;
        [SerializeField] private float playerExplodeSoundVolume = 0.2f;
        
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            AudioSource.PlayClipAtPoint(playerFireSound,Camera.main.transform.position,playerFireSoundVolume);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(playerExplodeSound,Camera.main.transform.position,playerExplodeSoundVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}