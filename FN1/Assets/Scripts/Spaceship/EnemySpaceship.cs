using System;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private AudioClip enemyFireSound;
        [SerializeField] private float enemyFireSoundVolume = 0.2f;
        [SerializeField] private AudioClip enemyExplodeSound;
        [SerializeField] private float enemyExplodeSoundVolume = 0.2f;
        [SerializeField] private double enemyFireRate = 0.5;
        private float fireCounter = 0f;
        
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(enemyFireRate > 0, "enemyFireRate has to be more than zero");            
        }
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(enemyExplodeSound,Camera.main.transform.position,enemyExplodeSoundVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            // TODO: Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter >= enemyFireRate)
            {
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
                AudioSource.PlayClipAtPoint(enemyFireSound,Camera.main.transform.position,enemyFireSoundVolume);
            }
        }
    }
}